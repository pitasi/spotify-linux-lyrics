#!/usr/bin/env python3

# Archlinux:
# yaourt -S playerctl
# Others:
# https://github.com/acrisci/playerctl

from gi.repository import Playerctl, GLib
from subprocess import Popen
import lyricwikia
import os

player = Playerctl.Player()

song = ('', '')
def on_track_change(player, e):
    artist = player.get_artist()
    title = player.get_title()
    global song
    if song == (artist, title): return
    else: song = (artist, title)

    message = '\n\n\n###\n###  {}\n###  - {}\n###\n\n{}'
    os.system('clear')
    try:
        lyrics = lyricwikia.get_lyrics(artist, title)
        print(message.format(title.upper(), artist, lyrics))
    except Exception as e:
        print(message.format(title.upper(), artist, 'Lyrics not found :('))

player.on('metadata', on_track_change)

GLib.MainLoop().run()
